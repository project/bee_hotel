<?php

namespace Drupal\beehotel_pricealterator\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Price Alterator Log' Block.
 *
 * @Block(
 *   id = "price_alterator_debug_block",
 *   admin_label = @Translation("price alterator debug block"),
 *   category = @Translation("price_alterator_debug_block"),
 * )
 */
class PriceAlteratorDebugBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $tempstore = \Drupal::service('tempstore.private')->get('beehotel_pricealterator');
    $output = "";

    $output .= date_create()->format('Uv') ." ";
    $output .= "beehotel_pricealterator_class".
                  $tempstore->get('beehotel_pricealterator_class')."<br>";

    return [
      '#markup' => $output,
    ];

  }

}
