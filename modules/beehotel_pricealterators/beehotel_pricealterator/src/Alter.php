<?php

namespace Drupal\beehotel_pricealterator;
use Drupal\Core\Session\AccountInterface;


/**
 * Class Alter
 * @package Drupal\beehotel_pricealterator\Services
 */
class Alter {

  protected $currentUser;

  /**
   * PriceAlteratorBaseTable constructor.
   * @param AccountInterface $currentUser
   */
  public function __construct(AccountInterface $currentUser) {
    $this->currentUser = $currentUser;
  }


  public function Alter($data)  {

    $tempstore = \Drupal::service('tempstore.private')->get('beehotel_pricealterator');

    // 1. Get content

    // 1a. Base Table with base price
    $basetable = \Drupal::service('beehotel_pricealterator.prealter')->BaseTable();
    //$season    = \Drupal::service('beehotel_pricealterator.prealter')->Season();

    // 1b. Normalise data
    $data = $this->Normalise($data);

    // 1c. Validate data
    $data = $this->Validate($data);




    // 1d. Available Alterators see......
    $pricealterator = \Drupal::service('plugin.manager.beehotel.pricealterator');
    $alterators = $pricealterator->SortedAlterators();

    // 1e. Enabled Alterators
    $alterators = $this->CheckStatus($alterators);

    $tempstore->set('beehotel_pricealterator_log', $alterators);


    /** 2. Loop per night
    * Drupal Commerce calls Resolver once per order item (not once per night)
    * We loop nights here, providing an average price to be multiplied by items (nights)
    * @TODO: keep track of evety night price into \$data
    */
    for ($n = 0; $n < $data['nights_as_integer']; $n++)  {

      $data['tmp'] = NULL;
      $data['tmp']['night_timestamp'] = $data['checkin_timestamp'] + (24*3600 * $n);
      $data['tmp']['night_date'] = date('Y-m-d', $data['tmp']['night_timestamp']);

      foreach ($alterators as $a) {
        $alterator = new $a['class']([],[],[],[]);


        // 3. get price for every night
        $data = $alterator->alter($data, $basetable);

        if (isset($data['tmp']['price'])) {
          $price[] = $data['tmp']['price'];
        }
      }

    }


    // 4. calculate average price
    $price = array_filter($price);
    if(count($price)) {
      $average = array_sum($price)/count($price);
    }

    //store the average as price per item (night)
    $data['amount'] = $average;


    // 5. return average price
    return $data;

  }


  private function CheckStatus($alterators)  {

    $a = [];

    // @TODO: move this into yml config
    $legenda = [
      0 => "disabled",
      1 => "enabled",
      2 => "work in progress",
    ];


    foreach ($alterators as $item)  {
      if ($item['status']  == 1 )
       $a[] = $item;
    }

    return $a;

  }


  private function Validate($data)  {

    if ($data['adults'] < 1 && $data['adults'] > 3  )  {
      // @TODO improve this check
      die ("ERR 34wfr: check occupancy");
    }

    return $data;

  }


  private function Normalise($data) {

    $data['checkin_timestamp'] = strtotime($data['checkin']);
    $data['checkout_timestamp'] = strtotime($data['checkout']);
    $data['nights'] = ($data['checkout_timestamp'] -  $data['checkin_timestamp'])/86400 ; //1 day = 86400
    $data['nights_as_integer'] = number_format($data['nights']);
    $data['checkin_timestamp'] = strtotime($data['checkin']);
    $data['day_of_the_week'] = date("N", $data['checkin_timestamp']); // 0 = Monday

    $tmp = $data['entity']->get('sku')->value;
    $data['id_room'] = strtoupper($tmp[0].$tmp[1]);

    return $data;

  }


  // How to send this output to the debug block?
  public function BeeHotelLog($data, $context) {

    //@TODO -  improve the price alterator log output
    return 0;

    $BEEHOTEL_DEBUG = 1;
    $tempstore = \Drupal::service('tempstore.private')->get('beehotel_pricealterator');

    if ($BEEHOTEL_DEBUG != 1) {die ("debug != 1");}

    $log = [];

    switch ($context['action']) {
      case 'begin':

        $tempstore->set('beehotel_pricealterator_price', $context['data']['price']);
        $tempstore->set('beehotel_pricealterator_function', $context['FUNCTION']);
        $tempstore->set('beehotel_pricealterator_class', $context['CLASS']);
        break;
      case 'step':
        break;
      case 'end':


        $tempstore->set('beehotel_pricealterator_price', $context['data']['price']);
        $tempstore->set('beehotel_pricealterator_function', $context['FUNCTION']);
        $tempstore->set('beehotel_pricealterator_class', $context['CLASS']);


        break;
      default:
          break;
          ;
      return;
    }
  }
}
