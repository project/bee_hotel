<?php

namespace Drupal\beehotel_pricealterator;

use Drupal\Core\Session\AccountInterface;


/**
 * Class PriceAlteratorBaseTable
 * @package Drupal\beehotel_pricealterator\Services
 */
class PreAlter {

  protected $currentUser;

  /**
   * PriceAlteratorBaseTable constructor.
   * @param AccountInterface $currentUser
   */
  public function __construct(AccountInterface $currentUser) {
    $this->currentUser = $currentUser;
  }


  /**
   * @return array
   */
  public function BaseTable() {

    $module_handler = \Drupal::moduleHandler();

    //get base table
    $basetable = $module_handler->invokeAll('beehotel_pricealterator_base_table', [[]]);


    if (!isset($basetable)) {
      dump ("\$basetable is required. Is at least beehotel_pricealterators_beehotel_pricealterator_base_table running");
      exit;
    }

    return $basetable;

  }


  /**
   * @return array
   */
  public function Season() {

    $module_handler = \Drupal::moduleHandler();

    //get season
    $season = $module_handler->invokeAll('beehotel_pricealterator_season', [[]]);


    if (!isset($season)) {
      dump ("\$season is required. Is at least beehotel_pricealterators_beehotel_pricealterator_season running?");
      exit;
    }

    return reset($season);

  }

}
