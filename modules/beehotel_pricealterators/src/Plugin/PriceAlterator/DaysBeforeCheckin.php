<?php

namespace Drupal\beehotel_pricealterators\Plugin\PriceAlterator;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\beehotel_pricealterator\PriceAlteratorBase;
use Drupal\beehotel_pricealterator\PriceAlteratorIntDerface;


/**
 * Provides a "Occupants" Price Alterator for BeeHotel.
 *
 * Because the plugin manager class for our plugins uses annotated class
 * discovery, Price Alterators only needs to exist within the
 * Plugin\PriceAlterator namespace, and provide a PriceAlterator annotation to be declared
 *  as a plugin. This is defined in
 * \Drupal\beehotel_pricealterator\PriceAlteratorPluginManager::__construct().
 *
 * The following is the plugin annotation. This is parsed by Doctrine to make
 * the plugin definition. Any values defined here will be available in the
 * plugin definition.
 *
 * This should be used for metadata that is specifically required to instantiate
 * the plugin, or for example data that might be needed to display a list of all
 * available plugins where the user selects one. This means many plugin
 * annotations can be reduced to a plugin ID, a label and perhaps a description.
 *
 *
 *  The weight Key is the weight for this alterator.
 * Legenda for the 'weight' key:
 * -9999 : heaviest, to be used as very first (reserved)
 * -9xxx : heavy, to be used as first (reserved)
 *     0 : no need to be weighted
 *  1xxx : allowed in custom modules
 *  xxxx : everything else
 *  9xxx : light, to be used as last (reserved)
 *  9999 : lightest, to be used as very last (reserved)
 *
 *
 * @PriceAlterator(
 *   description = @Translation("Price increase getting closer to checkin to give potential guests a sense of urgency when checkin the price the day after the first search"),
  *   id = "DaysBeforeCheckin",
 *   provider = "beehotel_pricealterator",
 *   type = "optional",
 *   status = 1,
 *   weight = 3,
 * )
 */
class DaysBeforeCheckin extends PriceAlteratorBase {



  /**
  * The type for this alterator
  *
  * @var string
  */
  const BEEHOTEL_TYPE = 'percentage';

  /**
   * The value for this alterator, between 0 and 100. Value 20 is a good option to give guests a sense of urgency when checkin prace the day after
   *
   * @var float
   */
  const BEEHOTEL_STATIC_INCREMENT = 20;

  /**
   * The value for this alterator
   *
   * @var int
   */
  const BEEHOTEL_DAYS_BEFORE = 40;

  /**
   * Alter a price.
   *
   * Every Alterator needs to have an  alter method
   *
   * @param array $data
   *   Array of data related to this price.
   *
   * @param array $pricetable
   *   Array of prices by week day.
   *
   * @return array $data
   *   An updated $data array.
   */
   public function alter(array $data, array $pricetable) {

    $elab['BEEHOTEL_STATIC_INCREMENT'] = self::BEEHOTEL_STATIC_INCREMENT;
    $elab['BEEHOTEL_DAYS_BEFORE'] = self::BEEHOTEL_DAYS_BEFORE;
    $elab['tmp'] = $data['checkin_timestamp'] - (60 * 60 * 24 * $elab['BEEHOTEL_DAYS_BEFORE']);
    $elab['days_first_day_to_increase'] = DrupalDateTime::createFromTimestamp($elab['tmp']) ->getTimestamp() ;
    $elab['days_today'] = DrupalDateTime::createFromTimestamp(time())->getTimestamp();

    //We technically should floor days_days_from_start_increase to define the very days as integers. While - in the real world -
    // leaving this as it is will give us a price increment  on a hourly basis as well. So nice :)
    $elab['days_days_from_start_increase'] = ($elab['days_today'] - $elab['days_first_day_to_increase']) /60/60/24;

    $elab['price_max_increase_same_day']  = ($data['tmp']['price']/100*self::BEEHOTEL_STATIC_INCREMENT);
    $elab['price_daily_increase']  = $elab['price_max_increase_same_day'] / $elab['BEEHOTEL_DAYS_BEFORE'] ;
    $elab['price_altered'] = $data['tmp']['price'] + ($elab['price_daily_increase'] * $elab['days_days_from_start_increase']) ;

    if (isset($elab['price_altered']) && $elab['price_altered'] > 0 ) {
      $data['tmp']['price'] = $elab['price_altered'];
      $data['alterator'][] = __CLASS__;
    }
    return $data;

  }

}
