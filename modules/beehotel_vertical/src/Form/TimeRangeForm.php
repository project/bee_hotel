<?php

/**
 * @file
 * Contains \Drupal\beehotel_vertical\Form\TimeRangeForm.
 */

namespace Drupal\beehotel_vertical\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\beehotel_vertical\Controller\Vertical;
use Drupal\Core\Render\Markup;

/**
 *
 */
class TimeRangeForm extends FormBase {
 
  
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'beehotel_vertical';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return "";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $request = \Drupal::request();
    $session = $request->getSession();
    $range =  $session->get('beehotel_requested_range');

    $items =     beehotel_vertical_timeranges();     //$this->timeranges();
    foreach ($items as $key =>  $item)  {
      $ranges[$key] = \Drupal::service('renderer')->render($item['label']);
    }
    
    $form['range'] = [
      '#title' => t('Time range'),
      '#description' => t("Which time range do want to see?"),
      '#type' => 'select',
      '#options' => $ranges,
      '#default_value' => $range,
    ];
      
    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#button_type' => 'primary',
    ];
    $form['#theme'] = 'vertical_form';
    return $form;
    
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    // We will need these values inside preprocess
    $session = $this->getRequest()->getSession();
    $session->set('beehotel_requested_range', $form_state->getValue('range'));
    
  }
  
}
