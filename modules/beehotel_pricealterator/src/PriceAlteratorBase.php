<?php

namespace Drupal\beehotel_pricealterator;

use Drupal\Component\Plugin\PluginBase;


/**
 * A very first class to get price from base table.
 *
 *
 * We intentionally declare our base class as abstract, and don't implement the
 * alter() method required by \Drupal\beehotel_pricealterator\PriceAlteratorInterface.
 * This way even if they are using our base class, developers will always be
 * required to define an alter() method for their custom pruce alterator.
 *
 * @see \Drupal\beehotel_pricealterator\Annotation\PriceAlterator
 * @see \Drupal\beehotel_pricealterator\PriceAlteratorInterface
 */
abstract class PriceAlteratorBase extends PluginBase implements PriceAlteratorInterface {


  /**
   * {@inheritdoc}
  */
  public function description() {
    // Retrieve the @description property from the annotation and return it.
    return $this->pluginDefinition['description'];
  }


  /**
   * {@inheritdoc}
  */
  public function status() {
    // Retrieve the @status property from the annotation and return it.
    return $this->pluginDefinition['status'];
  }


  /**
   * {@inheritdoc}
  */
  public function weight() {
    // Retrieve the @weight property from the annotation and return it.
    return $this->pluginDefinition['weight'];
  }


}
