CONTENT
-------

 * Introduction
 * Documentation
 * Maintainers


INTRODUCTION
------------

BEEHotel Price Alterators contains price alterators for the beehotel_price_alterator parent module.
Clone this module at whish to maintain your own list of Alterators



DOCUMENTATION
--------------
Updated Documentation is available at https://www.drupal.org/docs/contributed-modules/bee-hotel/bee-hotel-dynamic-prices-price-alterator



MAINTAINER
-----------

    * Augusto Fagioli(afagioli) - https://www.drupal.org/u/afagioli

    
