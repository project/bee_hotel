<?php

/**
 * @file
 * Hooks specific to the Bee Hotel module.
 */

/**
 * @addtogroup bee_hotel
 * @{
 *
 * The contents of this file are never loaded, or executed, it is purely for
 * documentation purposes.
 *
 *
 *
 * Give dynamic price to your room
 *
 * This hook is for implementation by bee hotel. In this hook,
 * the module let more modules to charge room price with custom logic

 * @param array &$data
 *   Price may rely on several variables collected in data. The function will return the amount value for a given night.
 *   The hook_bee_hotel_dynamic_price will set $data['amount']  with the price per night.
 *
 */

function hook_bee_hotel_dynamic_price(&$data) {

  //use default BEE price set in node/ID/edit
  $data['amount'] = $entity->get('price')->number;

}


/**
 * @} End of "addtogroup hooks_example".
 */
