<?php

namespace Drupal\beehotel_pricealterator;

/**
 * An interface for all PriceAlterator type plugins.
 *
 * When defining a new plugin type you need to define an interface that all
 * plugins of the new type will implement. This ensures that consumers of the
 * plugin type have a consistent way of accessing the plugin's functionality.
 *
 */
interface PriceAlteratorInterface {

  /**
   * Alter price into \$data.
   *
   *
   * @param array $data
   *   Array of data related to this price.
   *
   * @param array $pricetable
   *   Array of prices by week day.
   *
   * @return array $data
   *   An updated $data array.
   */
   public function alter(array $data, array $pricetable);

}
