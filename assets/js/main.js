(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.bee_hotel =  {
    attach: function (context, settings) {
      $(document, context).once('bee_hotel').each( function() {

        new Litepicker({
          element: document.getElementById('edit-dates'),
          singleMode: false,
          format: 'D MMM YYYY',
          tooltipText: {
            one: 'night',
            other: 'nights'
          },
          tooltipNumber: (totalDays) => {
            return totalDays - 1;
          }
        });
      });
    }
  }
} (jQuery, Drupal, drupalSettings));
