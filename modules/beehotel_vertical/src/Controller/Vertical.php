<?php
namespace Drupal\beehotel_vertical\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render;
use Drupal\Core\Render\Markup;
use Drupal\Component\Utility\Unicode;


/**
 * Provides route responses for the Example module.
 */
class Vertical extends ControllerBase {
  
  public function Result($data) { 
    
    $data['timeranges'] =  beehotel_vertical_timeranges();     
    $data['table_label'] =$data['timeranges'][ $data['range']]['label'];
    $data['rows'] = $this->rows($data) ;
    $data['header'] = $this->header($data) ;
    
    $output['table'] = [
      '#caption' => $data['table_label'],
      '#type' => 'table',
      '#attributes' => [
          'class' => ['vertical-table'],
        ],
      '#header' => $data['header'],
      '#rows' => $data['rows'],
      '#empty' =>t('Your table is empty'),
    ];
    return \Drupal::service('renderer')->render($output);

  }
  
  
  private function header($data){
    
    $data['units'] = bee_hotel_get_units ($options = ['status' => 1]);
    // n15d
      
    $header = [];
    $header[] = Markup::create('<strong>'.$this->t('Day').'</strong>');
    
    if (!empty($data['units'])) {
      foreach ($data['units'] as $unit) {
        $header[] = Markup::create("<h3 class='unit'>".$unit->GetTitle()."</h3>"); ;
      }
    }
    $data['header'] = $header;
    return $data['header'];
    
  }
  
  private function rows($data)  {
    
    $data['timeranges'] = beehotel_vertical_timeranges();
    $data['rowsnumber'] = $this->RowsNumber();
    $data['units'] = bee_hotel_get_units ($options = ['status' => 1]);
    
    
    
    // produce a row for every requested day
    for ($i = 0; $i <= $data['rowsnumber']; $i++)  {

      $data['day']['timestamp']  =  time() + ($i * (60*60*24) );
      
      // @TODO - move this into a twig template
      $data['day']['formatted'] = 
        "<span class='day-of-the-week'>" . 
          \Drupal::service('date.formatter')->format($data['day']['timestamp'], 'custom', 'D') .", 
        </span>" .
        
        "<span class='day-month'>" . 
         \Drupal::service('date.formatter')->format($data['day']['timestamp'], 'custom', 'd M') .",
        </span>" .
        
        "<span class='year'>" . 
         \Drupal::service('date.formatter')->format($data['day']['timestamp'], 'custom', 'Y') ."
        </span>" 
         ;
      
      $data['day']['d']      = \Drupal::service('date.formatter')->format($data['day']['timestamp'], 'custom', 'd');
      $data['day']['month'] = \Drupal::service('date.formatter')->format($data['day']['timestamp'], 'custom', 'n');
      $data['day']['m'] = \Drupal::service('date.formatter')->format($data['day']['timestamp'], 'custom', 'm');
      $data['day']['year'] = \Drupal::service('date.formatter')->format($data['day']['timestamp'], 'custom', 'Y');
      
      // ISO 8601 
      $data['day']['today']['ISO8601'] = $data['day']['year']."-".$data['day']['m']."-".$data['day']['d'];
      $data['day']['daybefore']['ISO8601'] = date('Y-m-d', strtotime("-1 day", strtotime($data['day']['today']['ISO8601'])));
      
      // produce columns
      $columns = [];
      
      // column 1 = day
      $columns[] = Markup::create('<strong>'.  $data['day']['formatted'] . '</strong>');
      
      // produce a column per unit
      
      if (!empty($data['units'])  ) {
        foreach ($data['units'] as $unit_id => $unit) {
          $data['unit']['node'] = $unit;
          $data['unit']['id'] = $unit_id ;
          $data = $this->CellContent($data);
          $columns[] = \Drupal::service('renderer')->render($data['cellcontent']);
        }
        $data['rows'][] = $columns;
        
      }
    }
    return $data['rows'];  
  }
  
  
  /**
  * Get time range for current report
  */  
  public function RequestedRange() {
    
    $request = \Drupal::request();
    $session = $request->getSession();
    $range =  $session->get('beehotel_requested_range');
    return $range;
    
  }

  /**
  * Get rows number for current report
  */  
  private function RowsNumber() {
    
    $data['range'] = $this->RequestedRange();
    $data['ranges'] = beehotel_vertical_timeranges();
    $data['rows'] = $data['ranges'][$data['range']]['rows'] ;
    return $data['rows'];
    
  }  
  
  // produce cell content (html + data )
  private function CellContent($data) {
    
    $data = $this->TypeofOccupacy($data);
    $data['cellcontent'] = $this->vertical_table_tr_td($data, $options = []);
    return $data;
    
  }

  
  /**
  * Check for event id existance in the montly BAT table
  *
  * Default template: bat-type-group-add-list.html.twig.
  *
  * @param array $data
  *   An associative array containing data about current vertcal report:
  *   - content: An array of type group bundles.
  * @returns Event ID | daily status
  */
  private function  TypeofOccupacy($data) {

    $database = \Drupal::database();
    $query = $database->select('bat_event_availability_daily_day_state', 'montlytable');
    
    // Fields
    $query->addField('montlytable', 'd' .($data['day']['d']*1)  , 'd'.($data['day']['d']*1) );

    // Conditions
    $query->condition('montlytable.unit_id', $data['unit']['id'], '='  );
    $query->condition('montlytable.year', $data['day']['year'], '='  );
    $query->condition('montlytable.month', $data['day']['month'], '=' );
    $query->condition('montlytable.d'. ($data['day']['d']*1) , 0, '<>' );
    $query->range(0, 1);
    

    $state_id = $query->execute()->fetchField(0);    
    
    $data['tmp']['event_id'] = bee_hotel_get_bat_event_id($data);
    
    
    // *****************************************************
    // Populate $data 
    
    //// Current 
    $data['occupancy']['current']['event']['id'] = $data['tmp']['event_id'] ;
    $data['occupancy']['current']['event']['state'] = $this->GetEventState($state_id);
    $data['occupancy']['current']['order'] = bee_hotel_from_event_to_order($data['occupancy']['current']['event']['id']);
    
    //// Given  ISO 8601/unit    
    $data['occupancy'][$data['day']['today']['ISO8601']][$data['unit']['id']]['event']['id'] = $data['tmp']['event_id'] ;
    $data['occupancy'][$data['day']['today']['ISO8601']][$data['unit']['id']]['event']['state'] = $this->GetEventState($state_id);
    
    $data['tmp']['order'] = bee_hotel_from_event_to_order($data['occupancy']['current']['event']['id']);
    $data['occupancy'][$data['day']['today']['ISO8601']][$data['unit']['id']]['order'] = $data['tmp']['order'] ;
    return $data;

  }
  
  private function GetEventState($state_id)  {
  
    $database = \Drupal::database();    
    $query = $database->select('states'  , 's');
    
    // Fields
    $query->addField('s','blocking');
    $query->addField('s','name');
    $query->addField('s','color');
    $query->addField('s','calendar_label');
    $query->addField('s','id');
    
    // Conditions
    $query->condition('s.id', $state_id, '='  );

    $result = $query->execute()->fetch();
    return $result ;
  }  
  
  
  private function EventIsBlocking($state_id)  {
    
    $is_blocking = FALSE;
    $database = \Drupal::database();    
    $query = $database->select('states'  , 's');
    
    // Fields
    $query->addField('s','blocking' );

    // Conditions
    $query->condition('s.id', $state_id, '='  );
    $query->range(0, 1);
    
    $result = $query->execute()->fetchField(0);
    
    return $is_blocking ;
  }
  
  
  private function GetOrderData($data, $options)  {
    
    $tmp = [];
    $tmp['day'] = $options['day'];
    $tmp['unit'] = $data['unit']['id'];
    $tmp['currentday_order_id'] = $this->GetCurrentDayOrderId($data, $tmp['unit']);
    $tmp['daybefore_order_id'] = $this->GetDayBeforeOrderId($data, $tmp['unit']);
            
    $order = [];
    $order['total_price__number'] = 0;
    
    if (!empty($data['occupancy'][ $tmp['day'] ][$tmp['unit']]) && !empty($data['occupancy'][ $tmp['day'] ][$tmp['unit']]['order'])) {
      $order['order_id'] = $data['occupancy'][$tmp['day']][$tmp['unit']]['order']->order_id;
      $order['mail'] = $data['occupancy'][$tmp['day']][$tmp['unit']]['order']->mail;
      $order['total'] = $data['occupancy'][$tmp['day']][$tmp['unit']]['order']->total_price__number;
      $order['order_number'] = $data['occupancy'][$tmp['day']][$tmp['unit']]['order']->order_number;
      return $order;
    }
    return ;
    
  }
  
  
  private function vertical_table_tr_td($data, $options) {
    
    $a_attributes = $this->vertical_table_tr_td_attributes($data, $options = ['div' => "a"]); 
    $a_class = implode ($a_attributes['class'], " ");
    $a_content = $this->vertical_order_item($data, $options = ['div' => "a"]);  
    
    $spacer_attributes = $this->vertical_table_tr_td_attributes($data, $options = ['div' => "spacer"]); 
    $spacer_class = implode ($spacer_attributes['class'], " ");
    $spacer_content = "";
    
    $b_attributes = $this->vertical_table_tr_td_attributes($data, $options = ['div' => "b"]); 
    $b_class = implode ($b_attributes['class'], " ");
    $b_content = $this->vertical_order_item($data, $options = ['div' => "b"]);
   
     return [
      '#theme' => 'vertical_table_tr_td', 
        '#a_class' => $a_class,
        '#a_content' => $a_content,
        '#spacer_class' => $spacer_class,
        '#spacer_content' => $spacer_content,
        '#b_class' => $b_class,
        '#b_content' => $b_content,
    ];
    
  }
  
  //build up order info
  private function vertical_order_item($data, $opt)  {

    $tmp= [];
    $tmp['unit'] = $data['unit']['id'];
    $tmp['currentday_order_id'] = $this->GetCurrentDayOrderId($data, $tmp['unit']);
    $tmp['daybefore_order_id'] = $this->GetDayBeforeOrderId($data, $tmp['unit']);
  
    $order = [];
    
    if (  $opt['div']  == "a" )  {
      $options = [
        'day' =>  $data['day']['daybefore']['ISO8601']  ,
        'div' =>  $opt['div'] ,
      ];
      $order = $this->GetOrderData($data, $options);
      
    }
    
    elseif ($opt['div']  == "b") {
      if ( ($tmp['daybefore_order_id'] != $tmp['currentday_order_id']) )  {
        $options = [
          'day' =>  $data['day']['today']['ISO8601'],
          'div' =>  $opt['div'] ,
        ];
        $order = $this->GetOrderData($data, $options);
      }
    }
    
    
    $mail = $order['mail'] ?? '';
    $total = $order['total_price__number'] ?? 0;
    
    return [
      '#theme' => 'vertical_order_item', 
      '#order_id' => $order['order_id'] ?? '' ,
      '#mail' => Unicode::truncate($mail, 8, FALSE, TRUE) ,
      '#total' => number_format($total),
      '#order_number' => $order['order_number'] ?? '',
    ];
    
    return;
    
  }
  
  //build up order info
  private function vertical_table_tr_td_attributes($data, $options)  {
   
    $attributes =['class' => [] ];
    $tmp['unit'] = $data['unit']['id'];
    $tmp['currentday_order_id'] = $this->GetCurrentDayOrderId($data, $tmp['unit']);
    $tmp['daybefore_order_id'] = $this->GetDayBeforeOrderId($data, $tmp['unit']);
    
    if (empty($data['occupancy'][ $data['day']['daybefore']['ISO8601'] ])) {
      $data['first_row_of_the_table'] = TRUE;
    }
    
    
    // A
    if ( $options['div'] == "a" ) {



        // A1. Same reservation today and yestarday
        // covered by A2 and A3
        //         if ( $tmp['currentday_order_id'] > 0 && ($tmp['currentday_order_id'] == $tmp['daybefore_order_id'])) {
        //           $attributes['class'][] = 'reservation';
        //         }

        // A2. Guest is checkin out today
        if (!empty($tmp['daybefore_order_id']) && $tmp['currentday_order_id'] !=  $tmp['daybefore_order_id']) {
          $attributes['class'][] = 'reservation';
          $attributes['class'][] = 'checkout';
        }

        // A3. Today's reservation is the same as yestarday
        if (!empty($tmp['daybefore_order_id']) && $tmp['currentday_order_id'] ==  $tmp['daybefore_order_id']) {
          $attributes['class'][] = 'reservation';
          $attributes['class'][] = 'same-as-daybefore';
        }

        // A4. Show Checkout on the first day in table
        // @TODO
      
      
        
    }
    
    
    
    
    // Spacer 
    if ( $options['div'] == "spacer" ) {
      
      //SPACER1. We have some reservation today and today reservation is not checkin in
      if (   !empty($data['occupancy'][ 'current']) && 
             !empty($data['occupancy']['current']['order']) 
             &&  $data['occupancy']['current']['order']->checkin != $data['day']['today']['ISO8601']
        ) {
        $attributes['class'][] =  'occupied';
      }
      
    }
    

    
    
    // B
    if ( $options['div'] == "b" ) {
  
      //B1. We have a reservation today
      if (!empty($data['occupancy'][ 'current']) && !empty($data['occupancy']['current']['order'])) {
        
        //dump ($data);exit;
        $attributes['class'][] =  'reservation';
        
        //B1.1. Today reservation is checkin today
        if ($data['occupancy']['current']['order']->checkin == $data['day']['today']['ISO8601'] ) {
          $attributes['class'][] =  'checkin';
        }
      }
      
    }
    return $attributes;
  }
  
  
  private function GetCurrentDayOrderId ($data, $unit)  {
    
    if (!empty($data)){
      if (!empty($data['occupancy'])){
        if (!empty($data['day']['today']['ISO8601'])){
          if (!empty($data['occupancy'][$data['day']['today']['ISO8601']])) {
           if (!empty($unit)) {
             if (!empty($data['occupancy'][$data['day']['today']['ISO8601']][$unit]['order'])) {
                if (!empty ($data['occupancy'][$data['day']['today']['ISO8601']][$unit]['order']) ) {
                  return $data['occupancy'][$data['day']['today']['ISO8601']][$unit]['order']->order_id;
                }
             }
           } 
          }
        }
      }
    }
    
  }

  
  private function GetDayBeforeOrderId ($data, $unit)  {
    
    if (!empty($data)){
      if (!empty($data['occupancy'])){
        if (!empty($data['day']['daybefore']['ISO8601'])){
          if (!empty($data['occupancy'][$data['day']['daybefore']['ISO8601']])) {
           if (!empty($unit)) {
             if (!empty($data['occupancy'][$data['day']['daybefore']['ISO8601']][$unit]['order'])) {
               return $data['occupancy'][$data['day']['daybefore']['ISO8601']][$unit]['order']->order_id;
             }
           } 
          }
        }
      }
    }
  }  
  
}
