<?php
namespace Drupal\bee_hotel_export\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\XmlResponse;



/**
 * Provides XML Data as Google Hotel directions
 */
class ExportAvailability extends ControllerBase {

  function Main()  {

    $options = [];
    $settings['now']       = time();
    $settings['m']         = $_GET['m'][0].$_GET['m'][1];
    $settings['y']         = $_GET['m'][2].$_GET['m'][3].$_GET['m'][4].$_GET['m'][5];
    $settings['currency']  = "EUR";
    
    $acceptable = $this->acceptable_months($settings);
    
    if (!in_array($settings['m'].$settings['y'], $acceptable)) {
      $output = $this->t("This page is provided by <a href='https://www.drupal.org/project/bee_hotel'>Bee Hotel</a>");
      return ['#markup' => $output];
    }
    
    $data = $this->GetMontlyData($settings);
    $xml  = $this->buildxml($data, $settings);

    if (strlen($xml) < 200 )  { $xml = ""; }

    $response = new Response();
    $response->setContent($xml);
    $response->headers->set('Content-Type', 'text/xml');
    return $response;

  }


   private function GetMontlyData($settings)  {

    $database = \Drupal::database();
    $query = $database->select('bat_event_availability_daily_day_event'  , ' tabella');
    $query->join('commerce_product'                                      , 'prod', 'tabella.unit_id = prod.product_id');
    $query->join('commerce_product__field_max_people'                    , 'max',  'tabella.unit_id = max.entity_id');


    // Fields
    $query->addField('tabella', 'unit_id'                     , 'u' );
    $query->addField('tabella', 'month'                       , 'm' );
    $query->addField('tabella', 'year'                        , 'y' );
    $query->addField('max'    , 'field_max_people_value'      , 'max' );

    for ($i = 1; $i <= 31; $i++)  {
        $query->addField('tabella', 'd' .$i      , 'd'.$i );
    }


    // Conditions
    $query->condition('tabella.year', $settings['y'], '='  );
    $query->condition('tabella.month', $settings['m'], '='  );


    $query->range(0, 1000);

    $result = $query->execute();

    // A - build array with rates
    foreach ($result as $record) {
        for ($i = 1; $i <= 31; $i++)  {
            $index = "d".$i;
            $data[$settings['y']][$settings['m']][$i][$record->max]['values'][] = $record->$index;
        }
    }


    // B - add avg
    foreach ($data[$settings['y']][$settings['m']] as $day => $rooms) {

        foreach ($rooms as $occupancy =>  $room) {

            $room['values'] = array_filter($room['values']);
            $average = array_sum($room['values'])/count($room['values']);
            $data[$settings['y']][$settings['m']][$day][$occupancy]['avg'] = $average;
        }
    }
    return $data;
  }



  //https://developers.google.com/hotels/hotel-prices/dev-guide/updating-prices
  private function buildxml($data, $settings)    {

   $tmp['timestamp'] = date(DATE_ATOM);

   $xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
   $xml .= "<Transaction timestamp=\"".date(DATE_ATOM)."\" id=\"123\">";

   foreach ($data as $item) {

    foreach ($data[$settings['y']][$settings['m']] as $day => $occupancies ){

      foreach ($occupancies  as $people =>  $occupancy ){
          $xml .= "<Result>";
          $xml .= "<Property>1234</Property>";
          $xml .= "<Checkin>".$settings['y']."-".$settings['m']."-".  str_pad($day, 2, '0', STR_PAD_LEFT)  ."</Checkin>";
          $xml .= "<Nights>1</Nights>";
          $xml .= "<Baserate currency=\"". $settings['currency']  ."\">".$occupancy['avg']."</Baserate>";
          $xml .= "<Tax currency=\"". $settings['currency']  ."\">0</Tax>";
          $xml .= "<OtherFees currency=\"". $settings['currency']  ."\">2.00</OtherFees>";
          $xml .= "<Occupancy>".$people."</Occupancy>";
          $xml .= "<OccupancyDetails>";
          $xml .= "<NumAdults>".$people."</NumAdults>";
          $xml .= "</OccupancyDetails>";
          $xml .= "</Result>";
      }
    }
  }
  $xml .= "</Transaction>";
  return $xml;
  }


  private function acceptable_months ($settings)  {
    $acceptables[] = date("mY");
    for  ($i = 1; $i < 12 ;$i++)  {
      $acceptables[]  = date( 'mY' , strtotime($settings['now'] . " +".$i." Months") );
    }
    return $acceptables;
  }

}
