<?php

// namespace Drupal\bee_hotel\Plugin\bee_hotel\field;
//
// use Drupal\Component\Utility\Xss;
// use Drupal\Core\Form\FormStateInterface;
// use Drupal\views\Render\ViewsRenderPipelineMarkup;
// use Drupal\views\Plugin\views\field\FieldPluginBase;
// use Drupal\views\ResultRow;
//
//
// /**
//  * A handler to provide a field that is completely custom by the administrator.
//  *
//  * @ingroup views_field_handlers
//  *
//  * @ViewsField("Beehcustom")
//  */
// class Beehcustom extends FieldPluginBase {
//
//   /**
//    * {@inheritdoc}
//    */
//   public function usesGroupBy() {
//     return FALSE;
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   public function query() {
//     // do nothing -- to override the parent query.
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   protected function defineOptions() {
//     $options = parent::defineOptions();
//
//     // Override the alter text option to always alter the text.
//     $options['alter']['contains']['alter_text'] = ['default' => TRUE];
//     $options['hide_alter_empty'] = ['default' => FALSE];
//     return $options;
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   public function buildOptionsForm(&$form, FormStateInterface $form_state) {
//     parent::buildOptionsForm($form, $form_state);
//
//     // Remove the checkbox
//     unset($form['alter']['alter_text']);
//     unset($form['alter']['text']['#states']);
//     unset($form['alter']['help']['#states']);
//     $form['#pre_render'][] = [$this, 'preRenderBeehcustomForm'];
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   public static function trustedCallbacks() {
//     $callbacks = parent::trustedCallbacks();
//     $callbacks[] = 'preRenderBeehcustomForm';
//     return $callbacks;
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   public function render(ResultRow $values) {
//     // Return the text, so the code never thinks the value is empty.
//     return ViewsRenderPipelineMarkup::create(Xss::filterAdmin($this->options['alter']['text']));
//     //return "My String";
//   }
//
//   /**
//    * Prerender function to move the textarea to the top of a form.
//    *
//    * @param array $form
//    *   The form build array.
//    *
//    * @return array
//    *   The modified form build array.
//    */
//   public function preRenderBeehcustomForm($form) {
//     $form['text'] = $form['alter']['text'];
//     $form['help'] = $form['alter']['help'];
//     unset($form['alter']['text']);
//     unset($form['alter']['help']);
//     return $form;
//   }
// }
