<?php
namespace Drupal\beehotel_pricealterator\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides route responses for the Example module.
 */
class Alterators extends ControllerBase {

  /**
   * Returns a list of ptice alterators.
   *
   * @return array
   *   A simple renderable array.
   */
  public function AlteratorsList() {

    $pricealterator = \Drupal::service('plugin.manager.beehotel.pricealterator');
    $alterators = $pricealterator->SortedAlterators();
    $rows = [];

    foreach  ($alterators as $a)  {
      $rows[] = [
        Markup::create('<strong>'.$a['id'].'</strong>'),
        $a['description'],
        $a['type'],
        $a['weight'],
        $a['status'],
      ] ;

      $rows[] = [
        [
          'data' => "Class: ".$a['class'],
          'colspan' => 5,
        ],
      ];
    }

    $header = [
      'title' => t('Id'),
      'description' => t('Description'),
      'type' => t('Type'),
      'weight' => t('Weight'),
      'status' => t('Status'),
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No content has been found.'),
      '#caption' => t('This table gives you a list of Price Alterator fund on this system'),
    ];


    $url = Url::fromUri('http://www.drupal.org/project/bee_hotel');
    $footer = Link::fromTextAndUrl(t("How to add your custom price alterators"), $url)->toString();

    return [
      '#type' => '#markup',
      '#markup' => render($build) . $footer ,

    ];
  }

}
