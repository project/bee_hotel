<?php

namespace Drupal\beehotel_pricealterator\Plugin\PriceAlterator;
use Drupal\beehotel_pricealterator\PriceAlteratorBase;

/**
 * Get the Base Price from the BaseTable.
 *
 * Because the plugin manager class for our plugins uses annotated class
 * discovery, Price Alterators only needs to exist within the
 * Plugin\PriceAlterator namespace, and provide a PriceAlterator annotation to be declared
 *  as a plugin. This is defined in
 * \Drupal\beehotel_pricealterator\PriceAlteratorPluginManager::__construct().
 *
 * The following is the plugin annotation. This is parsed by Doctrine to make
 * the plugin definition. Any values defined here will be available in the
 * plugin definition.
 *
 * This should be used for metadata that is specifically required to instantiate
 * the plugin, or for example data that might be needed to display a list of all
 * available plugins where the user selects one. This means many plugin
 * annotations can be reduced to a plugin ID, a label and perhaps a description.
 *
 *
 * The weight Key is the weight for this alterator
 * -9999 : heaviest, to be used as very first (reserved)
 * -9xxx : heavy, to be used as first (reserved)
 *     0 : no need to be weighted
 *  1xxx : allowed in custom modules (@TODO)
 *  xxxx : everything else
 *  9xxx : ligh, to be used as last (reserved)
 *  9999 : lighest, to be used as very last (reserved)
 *
 *
 * @PriceAlterator(
 *   description = @Translation("A very first step getting the price form the Base Table."),
 *   id = "PriceFromBaseTable",
 *   status = 1,
 *   type = "mandatory",
 *   weight = -9990,
  * )
 */
class PriceFromBaseTable extends PriceAlteratorBase {

  /**
   * Alter a price.
   *
   * Every Alterator needs to have an  alter method
   *
   * @param array $data
   *   Array of data related to this price.
   *
   * @param array $pricetable
   *   Array of prices by week day.
   *
   * @return array $data
   *   An updated $data array.
   */
   public function alter(array $data, array $basetable) {

    // Set the base (minimum)  price for  this request. This value will mainly increase with more alterators
    $data['tmp']['price'] = $basetable[$data['id_room']][$data['season']][$data['day_of_the_week']];
    return $data;

  }

}
