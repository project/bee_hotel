BEE Hotel SPS for Drupal
--------------
[BEE Hotel SPS]is a Drupal BEE Hotel (https://www.drupal.org/project/bee_hotel) sub-module that provides a slider for set alteration percentage for your Commerce store.

###Install

Installation
. enable module beehotel_sps
. enable module https://www.drupal.org/project/range_slider
. add field named "field_price_slider", type  "Number (integer)"


###Configure

Visit the Store configuration page and set the desidered value

