<?php

namespace Drupal\beehotel_pricealterator;

use Drupal\Core\Session\AccountInterface;


/**
 * Class PriceAlteratorBaseTable
 * @package Drupal\beehotel_pricealterator\Services
 */
class BaseTable  {

  protected $currentUser;

  /**
   * PriceAlteratorBaseTable constructor.
   * @param AccountInterface $currentUser
   */
  public function __construct(AccountInterface $currentUser) {
    $this->currentUser = $currentUser;
  }


  /**
   * @return array
   */
  public function BaseTable() {

    $module_handler = \Drupal::moduleHandler();

    //get base table from alterators container module
    $basetable = $module_handler->invokeAll('beehotel_pricealterator_base_table', [[]]);


    if (!isset($basetable)) {
      dump ("\$basetable is required. Is at least beehotel_pricealterators_beehotel_pricealterator_base_table running");
      exit;
    }

    return $basetable;

  }

}
