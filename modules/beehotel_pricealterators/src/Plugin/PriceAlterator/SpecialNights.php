<?php

namespace Drupal\beehotel_pricealterators\Plugin\PriceAlterator;

use Drupal\beehotel_pricealterator\PriceAlteratorBase;
use Drupal\beehotel_pricealterator\PriceAlteratorIntDerface;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;



/**
 * Provides a "SpecialNights" Price Alterator for BeeHotel
 *
 * Because the plugin manager class for our plugins uses annotated class
 * discovery, Price Alterators only needs to exist within the
 * Plugin\PriceAlterator namespace, and provide a PriceAlterator annotation to be declared
 *  as a plugin. This is defined in
 * \Drupal\beehotel_pricealterator\PriceAlteratorPluginManager::__construct().
 *
 * The following is the plugin annotation. This is parsed by Doctrine to make
 * the plugin definition. Any values defined here will be available in the
 * plugin definition.
 *
 * This should be used for metadata that is specifically required to instantiate
 * the plugin, or for example data that might be needed to display a list of all
 * available plugins where the user selects one. This means many plugin
 * annotations can be reduced to a plugin ID, a label and perhaps a description.
 *
 *
 *  The weight Key is the weight for this alterator.
 * Legenda for the 'weight' key:
 * -9999 : heaviest, to be used as very first (reserved)
 * -9xxx : heavy, to be used as first (reserved)
 *     0 : no need to be weighted
 *  1xxx : allowed in custom modules
 *  xxxx : everything else
 *  9xxx : light, to be used as last (reserved)
 *  9999 : lightest, to be used as very last (reserved)
 *
 *
 * @PriceAlterator(
 *   description = @Translation("Price increase when special nights occour"),
 *   doc = "",
 *   id = "SpecialNights",
 *   provider = "beehotel_pricealterator",
 *   type = "optional",
 *   status = 1,
 *   weight = 9004,
 * )
 */
class SpecialNights extends PriceAlteratorBase {

  /**
  * The type for this alterator
  *
  * @var string
  */
  const BEEHOTEL_TYPE = 'variable';

  /**
   * The value for this alterator
   *
   * @var float
   */
  const BEEHOTEL_STATIC_INCREMENT = 'variable';



  /**
   * Alter a price.
   *
   * Every Alterator needs to have an  alter method
   *
   * @param array $data
   *   Array of data related to this price.
   *
   * @param array $pricetable
   *   Array of prices by week day.
   *
   * @return array $data
   *   An updated $data array.
   */
   public function alter(array $data, array $pricetable) {

      $special_night = $this->IsSpecialNight($data['tmp']['night_date']);

      if (isset($special_night)) {
        $data['tmp']['price']  =  $data['tmp']['price'] + (self::BEEHOTEL_STATIC_INCREMENT) ;
      }
      $data['alterator'][] = __CLASS__;

      return $data;
    }


  /**
   * Is this a special night?
   *
   * Every Alterator needs to have an  alter method
   *
   * @param array $current_night
   *   Array of data related to this price.
   *
   * @return array $data
   *   An updated $data array.
   */

  function IsSpecialNight($current_night)  {

    $date = new DrupalDateTime($current_night);
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $formatted = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);


    $nid = \Drupal::entityQuery('node')
      ->condition('field_nights.value', $formatted, '=')
      ->range(0, 1)
      ->execute();

    if ($nid) {

      $node = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nid);
      $node = reset($node);

      $specialnight = [
        "title" =>  $node->getTitle(),
        "value" => $node->get('field_alteration')->value,
        "type" => $node->get('field_type')->value,
        "polarity" => $node->get('field_polarity')->value,
      ];

    }
    return $specialnight;

  }

}
