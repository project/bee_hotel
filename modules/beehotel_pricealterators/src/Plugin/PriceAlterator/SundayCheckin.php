<?php

namespace Drupal\beehotel_pricealterators\Plugin\PriceAlterator;

use Drupal\beehotel_pricealterator\PriceAlteratorBase;
use Drupal\beehotel_pricealterator\PriceAlteratorIntDerface;


/**
 * Provides a "Sunday Checkin" Price Alterator for BeeHotel
 *
 * Because the plugin manager class for our plugins uses annotated class
 * discovery, Price Alterators only needs to exist within the
 * Plugin\PriceAlterator namespace, and provide a PriceAlterator annotation to be declared
 *  as a plugin. This is defined in
 * \Drupal\beehotel_pricealterator\PriceAlteratorPluginManager::__construct().
 *
 * The following is the plugin annotation. This is parsed by Doctrine to make
 * the plugin definition. Any values defined here will be available in the
 * plugin definition.
 *
 * This should be used for metadata that is specifically required to instantiate
 * the plugin, or for example data that might be needed to display a list of all
 * available plugins where the user selects one. This means many plugin
 * annotations can be reduced to a plugin ID, a label and perhaps a description.
 *
 *
 *  The weight Key is the weight for this alterator.
 * Legenda for the 'weight' key:
 * -9999 : heaviest, to be used as very first (reserved)
 * -9xxx : heavy, to be used as first (reserved)
 *     0 : no need to be weighted
 *  1xxx : allowed in custom modules
 *  xxxx : everything else
 *  9xxx : light, to be used as last (reserved)
 *  9999 : lightest, to be used as very last (reserved)
 *
 *
 * @PriceAlterator(
 *   description = @Translation("Price increase when checkin in on Sunday"),
 *   doc = "",
 *   id = "SundayCheckin",
 *   provider = "beehotel_pricealterator",
 *   type = "optional",
 *   status = 1,
 *   weight = 9001,
 * )
 */
class SundayCheckin extends PriceAlteratorBase {



  /**
  * The type for this alterator
  *
  * @var string
  */
  const BEEHOTEL_TYPE = 'percentage';

  /**
   * The value for this alterator
   *
   * @var float
   */
  const BEEHOTEL_STATIC_INCREMENT = 40.00;



  /**
   * Alter a price.
   *
   * Every Alterator needs to have an  alter method
   *
   * @param array $data
   *   Array of data related to this price.
   *
   * @param array $pricetable
   *   Array of prices by week day.
   *
   * @return array $data
   *   An updated $data array.
   */
   public function alter(array $data, array $pricetable) {


      if ( (date ("N", $data['checkin_timestamp'])*1) == 7 ) {
        $data['tmp']['price']  =  $data['tmp']['price'] + (self::BEEHOTEL_STATIC_INCREMENT) ;
      }
      $data['alterator'][] = __CLASS__;

      return $data;
    }

}
