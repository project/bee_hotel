<?php

namespace Drupal\beehotel_pricealterator\Plugin\PriceAlterator;

use Drupal\beehotel_pricealterator\PriceAlteratorBase;
use Drupal\beehotel_pricealterator\Alter;
/**
 * Get the current price season.
 *
 * Because the plugin manager class for our plugins uses annotated class
 * discovery, Price Alterators only needs to exist within the
 * Plugin\PriceAlterator namespace, and provide a PriceAlterator annotation to be declared
 *  as a plugin. This is defined in
 * \Drupal\beehotel_pricealterator\PriceAlteratorPluginManager::__construct().
 *
 * The following is the plugin annotation. This is parsed by Doctrine to make
 * the plugin definition. Any values defined here will be available in the
 * plugin definition.
 *
 * This should be used for metadata that is specifically required to instantiate
 * the plugin, or for example data that might be needed to display a list of all
 * available plugins where the user selects one. This means many plugin
 * annotations can be reduced to a plugin ID, a label and perhaps a description.
 *
 *
 *
 * The weight Key is the weight for this alterator
 * -9999 : heaviest, to be used as very first (reserved)
 * -9xxx : heavy, to be used as first (reserved)
 *     0 : no need to be weighted
 *  1xxx : allowed in custom modules
 *  xxxx : everything else
 *  9xxx : ligh, to be used as last (reserved)
 *  9999 : lighest, to be used as very last (reserved)
 *
 *
 *
 * @PriceAlterator(
 *   id = "GetSeason",
 *   description = @Translation("Get the current business season to get the correct price from BaseTable"),
 *   type = "mandatory",
 *   weight = -9999,
 *   status = 1,
 * )
 */
class GetSeason extends PriceAlteratorBase {

  /**
   * The value for this alterator
   *
   * @var float
   */
  private $value = NULL;


  /**
   * The type for this alterator.
   * IE: "percentage", or "fixed"
   *
   * @var string
   */
  private $type = NULL;


  /**
   * Alter a price.
   *
   * Every Alterator needs to have an  alter method
   *
   * @param array $data
   *   Array of data related to this price.
   *
   * @param array $pricetable
   *   Array of prices by week day.
   *
   * @return array $data
   *   An updated $data array.
   */
   public function alter(array $data, array $pricetable) {

      $tmp =[];
      $tmp['date'] = $data['checkin'];
      $tmp['day'] = substr($tmp['date'], 8, 2);
      $tmp['month']   = substr($tmp['date'], 5, 2);

          if ($tmp['month'] == 1  && $tmp['day'] > 8    ) {$data['season'] = "low";}
      elseif ($tmp['month'] == 2                        ) {$data['season'] = "low";}
      elseif ($tmp['month'] == 3                        ) {$data['season'] = "low";}
      elseif ($tmp['month'] == 4  && $tmp['day']   > 15 ) {$data['season'] = "high";} // Easter?
      elseif ($tmp['month'] == 4                        ) {$data['season'] = "high";}
      elseif ($tmp['month'] == 5  OR $tmp['month'] == 6 ) {$data['season'] = "high";}
      elseif ($tmp['month'] == 7  && $tmp['day']   < 9  ) {$data['season'] = "high";}
      elseif ($tmp['month'] == 7  && $tmp['day']   > 8  ) {$data['season'] = "low";}
      elseif ($tmp['month'] == 8  && $tmp['day']   < 20 ) {$data['season'] = "low";}
      elseif ($tmp['month'] == 8  && $tmp['day']   > 19 ) {$data['season'] = "high";}
      elseif ($tmp['month'] == 9                        ) {$data['season'] = "high";}
      elseif ($tmp['month'] == 10 && $tmp['day']   < 19 ) {$data['season'] = "high";}
      elseif ($tmp['month'] == 10 && $tmp['day']   > 20 ) {$data['season'] = "low";}
      elseif ($tmp['month'] == 11                       ) {$data['season'] = "low";}
      elseif ($tmp['month'] == 12 && $tmp['day']   < 18 ) {$data['season'] = "low";}
      else                                                {$data['season'] = "high";}

      $context = [
        'action' => 'end',
        'CLASS' => __CLASS__,
        'FUNCTION' => __FUNCTION__,
        'data' => $data,
      ];

      $log = \Drupal::service('beehotel_pricealterator.alter')->BeeHotelLog($data, $context);

      return $data;
  }

}
