<?php


namespace Drupal\beehotel_ical\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Component\Utility\Unicode;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Response;





/**
 * Defines BeeHotelICal class.
 */
class BeeHotelICal extends ControllerBase {
  

  private function head($data) {

    $output  = "BEGIN:VCALENDAR\n";
    $output .= "PRODID:-//Drupal Bee Hotel//0.2 //EN\n";
    $output .= "VERSION:2.0\n";
    $output .= "CALSCALE:GREGORIAN\n";
    $output .= "METHOD:PUBLISH\n";
    $output .= "X-WR-CALNAME:BEEHotel-calendar\n";
    $output .= "X-WR-TIMEZONE:Europe/Rome\n";
    $output .= "X-WR-CALDESC:Druapl Bee Hotel Ical\n";
    return $output;

  }
  

  private function events($data) {
    
    $ical_config = \Drupal::config('beehotel_ical.settings')->get("ical");
    $host = \Drupal::request()->getHost();
    
    $days = [];
    for ($i = 1; $i < $data['howmanydays']; $i++ ) {
      $data['day']['d'] = date("d", strtotime('+'.$i.' days', strtotime($data['today'])));
      $data['day']['month'] = date("m", strtotime('+'.$i.' days', strtotime($data['today'])));
      $data['day']['year'] = date("Y", strtotime('+'.$i.' days', strtotime($data['today'])));

      
      $data['status'] = bee_hotel_get_day_status($data);
      $data['id'] = bee_hotel_get_bat_event_id($data);

      
      if (strpos ("0" . $ical_config['blocking_status'], $data['status'])) {
        
        $event = \Drupal::entityTypeManager()->getStorage('bat_event')->load($data['id']);

        $current_day = [];
        $current_day['DTSTAMP'] = $data['today']->format("Ymd\THms\Z");
        $current_day['UUID'] = $event->uuid()."@". $host;


        $current_day['DTSTART'] = \Drupal::service('date.formatter')->format( strtotime($event->get("event_dates")->value), 'custom', 'Ymd');
        $current_day['DTEND'] = \Drupal::service('date.formatter')->format( strtotime($event->get("event_dates")->end_value), 'custom', 'Ymd');


        $current_day['LAST-MODIFIED'] = \Drupal::service('date.formatter')->format( $event->get("changed")->value, 'custom', 'Ymd\THms\Z');   //;
        $current_day['CREATED'] = \Drupal::service('date.formatter')->format( $event->get("created")->value, 'custom', 'Ymd\THms\Z');   //;
        $current_day['Y-m-d'] = $data['day']['year']."-".$data['day']['month']."-".$data['day']['d'];
        $current_day['one_day_after'] = date("Ymd", strtotime('+1 days', strtotime($current_day['Y-m-d'])));
        
        if ($current_day['one_day_after'] == substr($current_day['DTEND'], 0, 8) )  {

          $current_day['ical']  = "BEGIN:VEVENT\r\n";
          $current_day['ical'] .= "DTSTART;VALUE=DATE:" . $current_day['DTSTART'] ."\r\n";
          $current_day['ical'] .= "DTEND;VALUE=DATE:". $current_day['DTEND'] ."\r\n";
          $current_day['ical'] .= "DTSTAMP:" . $data['today']->format("Ymd\THms\Z") . "\r\n";
          $current_day['ical'] .= "UID:". $current_day['UUID'] . "\r\n";
          $current_day['ical'] .= "CREATED:". $current_day['CREATED'] ."\r\n";
          $current_day['ical'] .= "DESCRIPTION:\r\n";
          $current_day['ical'] .= "LAST-MODIFIED:". $current_day['LAST-MODIFIED'] ."\r\n";
          $current_day['ical'] .= "LOCATION:\r\n";
          $current_day['ical'] .= "SEQUENCE:2\r\n";
          $current_day['ical'] .= "STATUS:CONFIRMED\r\n";
          $current_day['ical'] .= "SUMMARY:bbs bee_hotel\r\n";
          $current_day['ical'] .= "TRANSP:OPAQUE\r\n";
          $current_day['ical'] .= "END:VEVENT\r\n";
          $events .= $current_day['ical'] ;
        }
      }
    }
    
    return $events;
  }
  
  private function tail($data) {
    $output = "END:VCALENDAR\r\n";
    return $output;
  }

  
  public function availability( Node $node = NULL ) {

    $data = [];
    $data['unit']['id'] = $node->get("field_product")->target_id;
    $data['filename'] = "beehotel_".$this->cleanFileName($node->gettitle()).".ics";
    $data['type'] = "bat_event";  //
    $data['howmanydays'] = 10;

    $date = new DrupalDateTime( );
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $data['today'] = $date;
    $data['http_headers'] = $date ->format('D, d M Y G:i:s \G\M\T');

    $data['head'] = $this->head($data);
    $data['body'] = $this->events($data);
    $data['tail'] = $this->tail($data);
    
    $output = $data['head'] . $data['body'] . $data['tail'];

    $response = new Response();
    $response->headers->set('content-type', 'text/calendar');
    $response->headers->set('vary', 'Sec-Fetch-Dest, Sec-Fetch-Mode, Sec-Fetch-Site');
    $response->headers->set('cache-control', 'no-cache, no-store, max-age=0, must-revalidate');
    $response->headers->set('pragma', 'no-cache');
    $response->headers->set('expires', 'Mon, 01 Jan 1970 00:00:00 GMT');
    $response->headers->set('date', $data['http_headers']);
    
    // this is buggy
    //$response->headers->set('content-length', mb_strlen($data['body'], '8bit') );
    
    $response->headers->set('strict-transport-security', 'max-age=31536000; includeSubDomains; preload');
    $response->headers->set('cross-origin-opener-policy-report-only', 'same-origin-allow-popups; report-to="calendar_coop_coep"');
    $response->headers->set('cross-origin-embedder-policy-report-only', 'require-corp; report-to="calendar_coop_coep"');
    $response->headers->set('content-disposition', 'attachment; filename="'. $data['filename'] .'"');
    $response->setContent($output);
    return $response;
  }
  
  private function cleanFileName($file_name) { 
    
    $file_name_str = pathinfo($file_name, PATHINFO_FILENAME); 
     
    // Replaces all spaces with hyphens. 
    $file_name_str = str_replace(' ', '-', $file_name_str); 

    // Removes special chars. 
    $file_name_str = preg_replace('/[^A-Za-z0-9\-\_]/', '', $file_name_str); 
    
    // Replaces multiple hyphens with single one. 
    $file_name_str = preg_replace('/-+/', '-', $file_name_str); 
     
    return $file_name_str; 
  }
 
}
