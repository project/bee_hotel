<?php

/**
 * Respond to base price table being loaded with custom values.
 *
 * This hooks allows modules to replace bee hotel price alterator base price table with your own
 *
 */
function hook_beehotel_pricealterator_base_table($table) {

  return [];

}



/**
 * Provide a calendar array with days organized by season
 *
 * This hooks allows modules to define custom season days.
 * \$data contains year, which may be used to produce a more tailored result
 *
 */
function hook_beehotel_pricealterator_season($data) {

  return [];

}


