<?php

namespace Drupal\beehotel_pricealterator;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\beehotel_pricealterator\Annotation\PriceAlterator;
use Drupal\Core\Session\AccountInterface;


/**
 * A plugin manager for pricealterator plugins.
 *
 * The PriceAlteratorPluginManager class extends the DefaultPluginManager to provide
 * a way to manage pricealterator plugins. A plugin manager defines a new plugin type
 * and how instances of any plugin of that type will be discovered, instantiated
 * and more.
 *
 * Using the DefaultPluginManager as a starting point sets up our pricealterator
 * plugin type to use annotated discovery.
 *
 * The plugin manager is also declared as a service in
 * beehotel_pricealterator.services.yml so that it can be easily accessed and used
 * anytime we need to work with pricealterator plugins.
 */
class PriceAlteratorPluginManager extends DefaultPluginManager {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;


  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   *
   *
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, AccountInterface $current_user) {

    $subdir = 'Plugin/PriceAlterator';

    // The name of the interface that plugins should adhere to. Drupal will
    // enforce this as a requirement. If a plugin does not implement this
    // interface, Drupal will throw an error.
    $plugin_interface = PriceAlteratorInterface::class;

    // The name of the annotation class that contains the plugin definition.
    $plugin_definition_annotation_name = PriceAlterator::class;

    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);

    $this->alterInfo('pricealterator_info');
    $this->setCacheBackend($cache_backend, 'pricealterator_info', ['pricealterator_info']);
    $this->currentUser = $current_user;

  }

  /**
   * Gets alterators sorted by weight.
   *
   * @return array
   *   Returns an array of all alterators
   */
  public function SortedAlterators() {

    $alterators = $this->getDefinitions();
    $alterators = $this->WeightValidation($alterators);
    array_multisort(array_column($alterators, 'weight'), SORT_ASC, $alterators);
    return $alterators;

  }

  /**
   * Alterators from custom modules are only allowed with
   * weight 1xxx (from 1000 till 1999)
   *
   * @return array
   *   Returns an array of weight-validated alterators
   */
  public function WeightValidation($alterators) {

    //@TODO: implement validation
    return $alterators;

  }

}
