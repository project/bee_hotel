<?php
namespace Drupal\beehotel_searched\Controller;


use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime;
use Drupal\Core\Render\Markup;
use Drupal\node\Entity\Node;
use Drupal\Core\Render;


class Stats extends ControllerBase {


  function Main()  {

    $data['header'] = [
      $this->t('Day'),
      $this->t('DtA'),
      $this->t('Counter'),
    ];
 
    $data['tmp']  = "<h4>".$this->t('Legend')."</h4>";
    $data['tmp'] .= "<ul>";
      $data['tmp'] .= "<li><b>Day</b>: " . $this->t("Searched day by users");
      $data['tmp'] .= "<li><b>DtA</b>: " . $this->t("Day to Arrival");
      $data['tmp'] .= "<li><b>Counter</b>: " . $this->t("How many time Day was searched by users");
    $data['tmp'] .= "</ul>";
    
 
    $data['legend']  = Markup::create($data['tmp']);
    $data['rows'] = $this->searched_data();

 
    $output['table'] = [
      '#type' => 'table',
      '#caption' => $this->t("Most future searched nights"),
      '#attributes' => [
        'class' => ['stat-table'],
      ],
      '#header' => $data['header'],
      '#rows' => $data['rows'],
      '#empty'=> $this->t("Nothing to expose so far..."),
      
    ];  
    
    return ['#markup' => \Drupal::service('renderer')->render($output) . $data['legend']];

  }

  private function searched_data()  {
    
    $tmp = [];
    $tmp['today'] = \Drupal::time()->getRequestTime();    
    

    $database = \Drupal::database();
    $query = $database->select('node', 'n');

    // Join
    $query->join('node_field_data', 'nfd', 'n.nid = nfd.nid');
    $query->join('node__field_night', 'nfn', 'n.nid = nfn.entity_id');


    // Add fields
    $query->addField('nfn', 'field_night_value', 'night');


    // Count
    $query->addExpression('count(nfn.field_night_value)', 'counter');


    // Group
    $query->groupBy("nfn.field_night_value");


    // Condition 
    $query->condition('nfd.status', 1, '=');
    $query->condition('n.type', 'searched', '=');
    
    
    $query->condition('nfn.field_night_value', $tmp['today'], '>');

    // Sort
    
    $query->orderBy('counter', 'DESC');
    $query->orderBy('night', 'ASC');

    // Sort
    $query->range(0, 50);
    

    $result = $query->execute()->FetchAll();

    $rows = [];
    
    foreach ($result as $record) {
      
      $date_formatter = \Drupal::service('date.formatter');
      

      $tmp['night'] = new DrupalDateTime($record->night, 'UTC');
      
      $tmp['dta'] = $date_formatter->formatDiff($tmp['today'], $tmp['night']->getTimestamp(), [
        'granularity' => 3,
        'return_as_object' => TRUE,
      ])->toRenderable();      
      
      $rows[] = [
        'night' => Markup::create($tmp['night']->format('D') . "  " . "<b>".$tmp['night']->format('d') . "</b>  " . $tmp['night']->format('M Y')),
        'dta' => $tmp['dta']['#markup'],
        'counter' => $record->counter,
      ];

    }
    return $rows;   
  }

}
