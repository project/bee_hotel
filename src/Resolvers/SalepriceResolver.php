<?php

namespace Drupal\bee_hotel\Resolvers;

use Drupal\node\Entity\Node;
use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\Resolver\PriceResolverInterface;

/**
 * Class SalepriceResolver
 *
 * @package Drupal\bee_hotel\Resolvers
 */
class SalepriceResolver implements PriceResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function resolve(PurchasableEntityInterface $entity, $quantity, Context $context) {

    if ($entity->bundle() != 'bee') {return;}

    $cart_provider = \Drupal::service('commerce_cart.cart_provider');
    $stores = $entity->getProduct()->getStores();
    $store = reset($stores);
    $currency_code = $store->get('default_currency')->getValue()[0]['target_id'];
    $dates = bee_hotel_normalise_dates($_REQUEST['dates']);
    
    if (isset($dates['checkin']) && isset($dates['checkout']) && $_REQUEST['guests'] )  {
       
      $data                = Array();
      
      // 3rd digit Variant product is number of Guests
      // This setup is crucial for the "* person" calculation to work
      $data['adults']      = $_REQUEST['guests'][2];
      // ********************************************
      $data['check_in']    = $dates['checkin'];
      $data['check_out']   = $dates['checkout'];      
      $data['context']     = $context;
      $data['date']        = time();
      $data['entity']      = $entity;
      $data['nights']      = $quantity;
      $data['sku']         = $_REQUEST['guests'];

      \Drupal::moduleHandler()->invokeAll('bee_hotel_dynamic_price', [&$data]);
      $price = new Price($data['amount'], $currency_code);
      
      
      // Alter price with external services
      $moduleHandler = \Drupal::service('module_handler');
      if ($moduleHandler->moduleExists('beehotel_sps')) {
        $price = \Drupal::service('beehotel_sps.apply_price_slider')->apply($data['amount'], $store);
      }
      
      return $price;
    }
  }
}
