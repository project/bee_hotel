CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Conflicts/Known issues
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

“BeeHotel Searched” module stores information about Guests search on your website and produce valuable information to increase revenues in the hospitality business.




REQUIREMENTS
------------

This module requires Drupal core, Commerce modules, BAT, BEE, BeeHotel.


CONFLICTS/KNOWN ISSUES
----------------------

None



INSTALLATION
------------

 * Install the CAPTCHA module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.



TODO
----

    . Produce more detailed reports
    . Integrate with the "BeeHotel Dynamic rates"


MAINTAINERS
-----------

   * Augusto Fagioli (afagioli) - https://www.drupal.org/u/afagioli
   

Supporting organizations:

 * Fagioli.biz - https://fagioli.biz

 
