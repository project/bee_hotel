<?php
namespace Drupal\beehotel_event\Util;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Datetime\DrupalDateTime;


/**
 * Defines  the Utility EventMaintenance class.
 */
class EventMaintenance  {

  public function delete_old_bat_events ($options)   {

    if (!isset($options['daysago'])) {
      $options['daysago'] = 20; 
    }

    if (!isset($options['howmany']) ) {
      $options['howmany'] = 600; 
    }

    $date = new DrupalDateTime( $options['daysago'] . ' days ago');
    $date->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $formatted = $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    $query = \Drupal::entityQuery('bat_event');
    $count_pre = $query->count()->execute();

    $ids = \Drupal::entityQuery('bat_event')
      ->condition('event_dates.value', $formatted, '<')
      ->range(0, $options['howmany'])
      ->execute();
    bat_event_delete_multiple($ids);

    $query = \Drupal::entityQuery('bat_event');
    $count_post = $query->count()->execute();

    $message = t("counter_pre : [ %counter_pre ].N. %c bat_event(s)  older than %older days deleted. %remain bat_event(s) still in DB", ["%c" => $options['howmany'], "%older" => $options['daysago'], "%remain" => $count_post , "%count_pre" => $count_pre ]);

    \Drupal::logger('beehotel_event')->notice($message);
    return;

  }
}
