CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Maintainer


INTRODUCTION
------------

BEE Hotel integrates BEE with features required in the most common Hotel use cases.

 * For a full description of the module visit:
   https://www.drupal.org/project/bee_hotel

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/bee_hotel


REQUIREMENTS
------------

This module requires the following contributed modules.

 * VIEWS - Drupal Core
 * BEE   - https://www.drupal.org/project/bee



RECOMMENDED MODULES
-------------------

 * Commerce - https://www.drupal.org/project/commerce
 * Commerce Pricelist  - https://www.drupal.org/project/commerce_pricelist

INSTALLATION
------------

 * Install the module as you would normally install a contributed Drupal module. It is highly recommended to use
   composer to install Bee Hotel, as that will fetch all dependencies automatically.
   

CONFIGURATION
-------------

  As the whole Commerce enviroment, BEE_Hotel is quite a lot about configuration. We believe we all should contribute to keep the Documentation https://www.drupal.org/docs/contributed-modules/bee-hotel up to date. Thanks for your support!

  

WISH LIST
---------

    . Form "Book Now" > define max checkout time, charging for extra time
    . Reservation  flow > add upsell page
    . Set a defined amount of money  in commerce_papal to complete the order with a partial payment
    . Add UI for editing OrderDetailsPane content

MAINTAINER
-----------

 * Augusto Fagioli(augustofagioli) - https://www.drupal.org/u/augustofagioli

    
