<?php

namespace Drupal\beehotel_pricealterators\Plugin\PriceAlterator;

use Drupal\beehotel_pricealterator\PriceAlteratorBase;
use Drupal\beehotel_pricealterator\PriceAlteratorInterface;



/**
 * Provides a "One Night Only" Price Alterator for BeeHotel.
 *
 * Because the plugin manager class for our plugins uses annotated class
 * discovery, Price Alterators only needs to exist within the
 * Plugin\PriceAlterator namespace, and provide a PriceAlterator annotation to be declared
 *  as a plugin. This is defined in
 * \Drupal\beehotel_pricealterator\PriceAlteratorPluginManager::__construct().
 *
 * The following is the plugin annotation. This is parsed by Doctrine to make
 * the plugin definition. Any values defined here will be available in the
 * plugin definition.
 *
 * This should be used for metadata that is specifically required to instantiate
 * the plugin, or for example data that might be needed to display a list of all
 * available plugins where the user selects one. This means many plugin
 * annotations can be reduced to a plugin ID, a label and perhaps a description.
 *
 *
 *  The weight Key is the weight for this alterator.
 * Legenda for the 'weight' key:
 * -9999 : heaviest, to be used as very first (reserved)
 * -9xxx : heavy, to be used as first (reserved)
 *     0 : no need to be weighted
 *  1xxx : allowed in custom modules (@TODO)
 *  xxxx : everything else
 *  9xxx : light, to be used as last (reserved)
 *  9999 : lightest, to be used as very last (reserved)
 *
 *
 * @PriceAlterator(
 *   description = @Translation("A very first step getting the price form the Base Table ."),
 *   id = "OneNightOnly",
 *   provider = "beehotel_pricealterator",
 *   type = "optional",
 *   status = 1,
 *   weight = 1,
 * )
 */
class OneNightOnly extends PriceAlteratorBase {


  /**
  * The type for this alterator
  *
  * @var string
  */
  const BEEHOTEL_TYPE = 'percentage';

  /**
   * The value for this alterator
   *
   * @var float
   */
  const BEEHOTEL_VALUE = 60.00;


  /**
   * Alter a price.
   *
   * Every Alterator needs to have an  alter method
   *
   * @param array $data
   *   Array of data related to this price.
   *
   * @param array $pricetable
   *   Array of prices by week day.
   *
   * @return array $data
   *   An updated $data array.
   */
   public function alter(array $data, array $pricetable) {

      if ($data['nights'] < 2) {
        if (static::BEEHOTEL_TYPE == 'percentage' && static::BEEHOTEL_VALUE <> 0 )  {
          $data['tmp']['price'] = $data['tmp']['price'] + ($data['tmp']['price'] / 100 * static::BEEHOTEL_VALUE);
        }
      }
      $data['alterator'][] = __CLASS__;
      return $data;
    }
}
