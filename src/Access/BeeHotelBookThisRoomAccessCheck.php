<?php

namespace Drupal\bee_hotel\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;

class BeeHotelBookThisRoomAccessCheck implements AccessInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a BeeHotelBookThisRoomAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return string
   *   A \Drupal\Core\Access\AccessInterface constant value.
   */
  public function access(AccountInterface $account, Node $node = NULL) {


    //echo "anonimo nonpuò prenotare";
    //dump ($node);exit;

    if ($node) {
      $bee_hotel_settings = \Drupal::config('node.type.' . $node->bundle())->get('bee');
      //dump ($bee_hotel_settings);exit;




      if (isset($bee_hotel_settings['bookable']) && $bee_hotel_settings['bookable']) {

        // everybody is welcome to book my rooms :)
        return AccessResult::allowed();

      }
    }
    return AccessResult::forbidden();
  }

}
